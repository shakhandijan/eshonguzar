import React from 'react'
import Group529 from '../../images/muslim png.png'
import back from '../../images/back.png'
import back2 from '../../images/back2.png'
import back3 from '../../images/backtop.png'
import './AbuUmoma.css'


const AbuUmoma = () => {
    return (
        <div className="container">
            <div className="images">
                <img src={Group529} alt="" id="group529"/>
                <img src={back} alt="" id="back"/>
                <img src={back2} alt="" id="back2"/>
                <img src={back3} alt="" id="back3"/>
            </div>
        </div>
    )
}

export default AbuUmoma;